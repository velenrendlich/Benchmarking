﻿using System;
using System.Runtime.CompilerServices;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	// We're bound by the cost of operation, not the cost of branch, lol! Let this be a lesson for you!
	// Branches are there to do less work!
	public class IsUnpredictableBranchBad
	{
		public struct EntityData
		{
			public bool Value;
			public double Result;
		}

		int Count = 100_000;

		EntityData[] AlwaysProcessed;
		EntityData[] Mispredicted;
		EntityData[] GoodPredicted;
		EntityData[] WellPredicted;
		EntityData[] VeryWellPredicted;
		EntityData[] NearPerfectPredicted;
		EntityData[] PerfectPredicted;

		public IsUnpredictableBranchBad()
		{
			AlwaysProcessed = new EntityData[Count];
			Mispredicted = new EntityData[Count];
			GoodPredicted = new EntityData[Count];
			WellPredicted = new EntityData[Count];
			VeryWellPredicted = new EntityData[Count];
			NearPerfectPredicted = new EntityData[Count];
			PerfectPredicted = new EntityData[Count];

			var random = new Random(0);

			for(int i = 0; i < Count; i++)
			{
				var val = random.NextDouble();
				AlwaysProcessed[i].Result = val;
				Mispredicted[i].Result = val;
				GoodPredicted[i].Result = val;
				WellPredicted[i].Result = val;
				VeryWellPredicted[i].Result = val;
				NearPerfectPredicted[i].Result = val;
				PerfectPredicted[i].Result = val;

				Mispredicted[i].Value = random.NextDouble() > 0.5f;
				GoodPredicted[i].Value = random.NextDouble() > 0.25f;
				WellPredicted[i].Value = random.NextDouble() > 0.1f;
				VeryWellPredicted[i].Value = random.NextDouble() > 0.01f;
				NearPerfectPredicted[i].Value = random.NextDouble() > 0.001f;
				PerfectPredicted[i].Value = true;
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		double Process(EntityData[] array)
		{
			var result = 0d;

			for(int i = 0; i < array.Length; i++)
			{
				if(array[i].Value)
				{
					result += Math.Sqrt(array[i].Result);
				}
			}

			return result;
		}

		[Benchmark(Baseline = true)]
		public double ProcessMispredicted()
		{
			return Process(Mispredicted);
		}

		[Benchmark]
		public double ProcessGoodPredicted()
		{
			return Process(GoodPredicted);
		}
		
		[Benchmark]
		public double ProcessWellPredicted()
		{
			return Process(WellPredicted);
		}
		
		[Benchmark]
		public double ProcessVeryWellPredicted()
		{
			return Process(VeryWellPredicted);
		}
		
		[Benchmark]
		public double ProcessNearPerfectPredicted()
		{
			return Process(NearPerfectPredicted);
		}
		
		[Benchmark]
		public double ProcessPerfectPredicted()
		{
			return Process(PerfectPredicted);
		}

		[Benchmark]
		public double ProcessAlways()
		{
			var result = 0d;

			for(int i = 0; i < Count; i++)
			{
				result += Math.Sqrt(AlwaysProcessed[i].Result);
			}

			return result;
		}
	}
}