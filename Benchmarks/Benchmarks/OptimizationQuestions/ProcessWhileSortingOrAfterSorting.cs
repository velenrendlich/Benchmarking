﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	public class ProcessWhileSortingOrAfterSorting
	{
		public struct EntityData
		{
			public bool Value;
			public double Result;
		}

		int Count = 100_000;
		EntityData[] Data;
		List<int> Indices;
		
		public ProcessWhileSortingOrAfterSorting()
		{
			Data = new EntityData[Count];
			Indices = new List<int>(Count);

			var random = new Random(0);
			
			for(int i = 0; i < Count; i++)
			{
				Data[i] = new EntityData
				{
					Result = random.NextDouble(),
					Value = random.NextDouble() > 0.5f,
				};
			}
		}

		[Benchmark(Baseline = true)]
		public double ProcessWhileSorting()
		{
			var result = 0d;
			
			for(int i = 0; i < Count; i++)
			{
				if(Data[i].Value)
				{
					result += Math.Sqrt(Data[i].Result);
				}
			}

			return result;
		}
		
		[Benchmark]
		public double ProcessAfterSorting()
		{
			Indices.Clear();
			var result = 0d;
			
			for(int i = 0; i < Count; i++)
			{
				if(Data[i].Value)
				{
					Indices.Add(i);
				}
			}

			for(int i = 0; i < Indices.Count; i++)
			{
				result += Math.Sqrt(Data[Indices[i]].Result);
			}

			return result;
		}
	}
}