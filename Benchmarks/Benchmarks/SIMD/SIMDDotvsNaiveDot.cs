﻿using System;
using System.Numerics;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	[ShortRunJob]
	public class SIMDDotvsNaiveDot
	{
		public struct float4
		{
			public float x;
			public float y;
			public float z;
			public float w;
		}

		public class float4SoA
		{
			public float[] xs;
			public float[] ys;
			public float[] zs;
			public float[] ws;

			public float4SoA(int count)
			{
				xs = new float[count];
				ys = new float[count];
				zs = new float[count];
				ws = new float[count];
			}
		}

		const int Count = 16 * 1_000_000;
		static float4[] Float4s = new float4[Count];
		static float4SoA SoA = new(Count);
		static Random Random = new();
		static float[] Result = new float[Count];
		
		[GlobalSetup]
		public void Setup()
		{
			for(int i = 0; i < Count; i++)
			{
				var x = Random.Next();
				var y = Random.Next();
				var z = Random.Next();
				var w = Random.Next();

				Float4s[i] = new float4
				{
					x = x,
					y = y,
					z = z,
					w = w,
				};

				SoA.xs[i] = x;
				SoA.ys[i] = y;
				SoA.zs[i] = z;
				SoA.ws[i] = w;
			}
		}

		[Benchmark]
		public void BenchmarkNaiveFloat4LengthSq()
		{
			NaiveFloat4LengthSq();
		}

		[Benchmark(Baseline = true)]
		public void BenchmarkSIMDFloat4LengthSq()
		{
			SIMDFloat4LengthSq();
		}

		[Benchmark]
		public void BenchmarkSoALengthSq()
		{
			SoALengthSq();
		}

		public static bool CheckIfResultsAreSame()
		{
			SIMDFloat4LengthSq();
			var temp1 = new float[Count];
			Result.CopyTo(temp1, 0);

			NaiveFloat4LengthSq();
			var temp2 = new float[Count];
			Result.CopyTo(temp2, 0);

			for(int i = 0; i < Count; i++)
			{
				if(Math.Abs(temp1[i] - temp2[i]) > 0.001f)
				{
					return false;
				}
			}

			return true;
		}
		
		public static void SIMDFloat4LengthSq()
		{
			var simdLength = Vector<float>.Count;
			int i;
			for(i = 0; i <= Count - simdLength; i += simdLength)
			{
				var vx = new Vector<float>(SoA.xs, i);
				var vy = new Vector<float>(SoA.ys, i);
				var vz = new Vector<float>(SoA.zs, i);
				var vw = new Vector<float>(SoA.ws, i);
				var xm = Vector.Multiply(vx, vx);
				var ym = Vector.Multiply(vy, vy);
				var zm = Vector.Multiply(vz, vz);
				var wm = Vector.Multiply(vw, vw);
				var result = Vector.Add(xm, Vector.Add(ym, Vector.Add(zm, wm)));
				result.CopyTo(Result, i);
			}
		}
		
		public static void NaiveFloat4LengthSq()
		{
			for(int i = 0; i < Count; i++)
			{
				var v = Float4s[i];
				Result[i] = v.x * v.x + v.y * v.y + v.z * v.z + v.w + v.w;
			}
		}
		
		
		public static void SoALengthSq()
		{
			for(int i = 0; i < Count; i++)
			{
				Result[i] = SoA.xs[i] * SoA.xs[i] + SoA.ys[i] * SoA.ys[i] + SoA.zs[i] * SoA.zs[i] + SoA.ws[i] + SoA.ws[i];
			}
		}
	}
}