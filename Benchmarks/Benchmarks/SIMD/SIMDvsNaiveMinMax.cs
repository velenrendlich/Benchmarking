﻿using System;
using System.Numerics;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	[ShortRunJob]
	public class SIMDvsNaiveMinMax
	{
		ushort[] Input = new ushort[16 * 1_000_000];
		Random Random = new Random();
		
		[IterationSetup]
		public void Setup()
		{
			for(int i = 0; i < Input.Length; i++)
			{
				Input[i] = (ushort)Random.Next();
			}
		}
		
		[Benchmark(Baseline = true)]
		public void BenchmarkSIMDMinMax()
		{
			SIMDMinMax(Input, out var min, out var max);
			var result = min + max;
		}

		[Benchmark]
		public void BenchmarkNaiveMinMax()
		{
			NonSIMDMinMax(Input, out var min, out var max);
			var result = min + max;
		}
		
		public static void SIMDMinMax(ushort[] input, out ushort min, out ushort max) {
			var simdLength = Vector<ushort>.Count;
			var vmin = new Vector<ushort>(ushort.MaxValue);
			var vmax = new Vector<ushort>(ushort.MinValue);
			var i = 0;

			// Find the max and min for each of Vector<ushort>.Count sub-arrays 
			for (i = 0; i <= input.Length - simdLength; i += simdLength) {
				var va = new Vector<ushort>(input, i);
				vmin = Vector.Min(va, vmin);
				vmax = Vector.Max(va, vmax);
			}

			// Find the max and min of all sub-arrays
			min = ushort.MaxValue;
			max = ushort.MinValue;
			for (var j = 0; j < simdLength; ++j) {
				min = Math.Min(min, vmin[j]);
				max = Math.Max(max, vmax[j]);
			}

			// Process any remaining elements
			for (; i < input.Length; ++i) {
				min = Math.Min(min, input[i]);
				max = Math.Max(max, input[i]);
			}
		}
		
		public static void NonSIMDMinMax(ushort[] input, out ushort min, out ushort max) {
			min = ushort.MaxValue;
			max = ushort.MinValue;
			foreach (var value in input) {
				min = Math.Min(min, value);
				max = Math.Max(max, value);
			}
		}
	}
}