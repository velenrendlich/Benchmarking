﻿using System;
using System.Buffers;
using System.Text;
using BenchmarkDotNet.Attributes;

namespace Benchmarks.String
{
	//[HardwareCounters(HardwareCounter.BranchMispredictions, HardwareCounter.InstructionRetired, HardwareCounter.CacheMisses)]
	[ShortRunJob]
	[MemoryDiagnoser]
	public class StringBenchmarks
	{
		string[] RandomStringAs;
		string[] RandomStringBs;
		string[] RandomStringCs;

		int Count = 100_000;
		int StringSizeMin = 1;
		int StringSizeMax = 10;

		public StringBenchmarks()
		{
			var alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var random = new Random(0);

			RandomStringAs = new string[Count];
			RandomStringBs = new string[Count];
			RandomStringCs = new string[Count];

			for(int i = 0; i < Count; i++)
			{
				var char1 = random.Next(StringSizeMin, StringSizeMax);
				var chars = new char[char1];
				for(int j = 0; j < char1; j++)
				{
					chars[j] = alphanumeric[random.Next(alphanumeric.Length)];
				}

				RandomStringAs[i] = new string(chars);

				var char2 = random.Next(StringSizeMin, StringSizeMax);
				chars = new char[char2];
				for(int j = 0; j < char2; j++)
				{
					chars[j] = alphanumeric[random.Next(alphanumeric.Length)];
				}

				RandomStringBs[i] = new string(chars);
				var char3 = random.Next(StringSizeMin, StringSizeMax);
				chars = new char[char3];
				for(int j = 0; j < char3; j++)
				{
					chars[j] = alphanumeric[random.Next(alphanumeric.Length)];
				}

				RandomStringCs[i] = new string(chars);
			}
		}

		[Benchmark]
		public string[] StringCreate()
		{
			var result = new string[Count];

			for(int i = 0; i < Count; i++)
			{
				var strA = RandomStringAs[i];
				var strB = RandomStringBs[i];
				var strC = RandomStringCs[i];
				var resultLength = strA.Length + strB.Length + strC.Length + 2;
				
				result[i] = string.Create(resultLength, (RandomStringAs[i], RandomStringBs[i], RandomStringCs[i]), (chars, state) =>
				{
					var count = 0;
					var (item1, item2, item3) = state;
					item1.AsSpan().CopyTo(chars);
					count += item1.Length;
					chars[count++] = '\n';
					item2.AsSpan().CopyTo(chars[count..]);
					count += item2.Length;
					chars[count++] = '\n';
					item3.AsSpan().CopyTo(chars[count..]);
				});
			}

			return result;
		}

		[Benchmark]
		public string[] StringStackAlloc()
		{
			var result = new string[Count];
			Span<char> spanMaxLength = stackalloc char[StringSizeMax * 3 + 10];

			for(int i = 0; i < Count; i++)
			{
				var countA = RandomStringAs[i].Length;
				var countB = RandomStringBs[i].Length;
				var countC = RandomStringCs[i].Length;

				var count = 0;
				RandomStringAs[i].AsSpan().CopyTo(spanMaxLength.Slice(count, countA));
				count += countA;
				spanMaxLength[count++] = '\n';
				RandomStringBs[i].AsSpan().CopyTo(spanMaxLength.Slice(count, countB));
				count += countB;
				spanMaxLength[count++] = '\n';
				RandomStringCs[i].AsSpan().CopyTo(spanMaxLength.Slice(count, countC));
				count += countC;

				result[i] = spanMaxLength[..count].ToString();
			}

			return result;
		}

		[Benchmark]
		public string[] StringArrayPool()
		{
			var result = new string[Count];

			for(int i = 0; i < Count; i++)
			{
				var countA = RandomStringAs[i].Length;
				var countB = RandomStringBs[i].Length;
				var countC = RandomStringCs[i].Length;
				var totalCount = countA + countB + countC + 2;
				var array = ArrayPool<char>.Shared.Rent(totalCount);
				var index = 0;
				RandomStringAs[i].CopyTo(0, array, index, countA);
				index += countA;
				array[index++] = '\n';
				RandomStringBs[i].CopyTo(0, array, index, countB);
				index += countB;
				array[index++] = '\n';
				RandomStringCs[i].CopyTo(0, array, index, countC);

				result[i] = array.AsSpan(0, totalCount).ToString();

				ArrayPool<char>.Shared.Return(array);
			}

			return result;
		}

		[Benchmark]
		public string[] CreateStringWithBuilder()
		{
			var result = new string[Count];

			var capacity = StringSizeMax * 3 + 10;
			var builder = new StringBuilder(capacity);

			for(int i = 0; i < Count; i++)
			{
				builder.Append(RandomStringAs[i]);
				builder.Append('\n');
				builder.Append(RandomStringBs[i]);
				builder.Append('\n');
				builder.Append(RandomStringCs[i]);
				result[i] = builder.ToString();

				builder.Clear();
			}

			return result;
		}

		[Benchmark]
		public string[] CreateStringConcatenation()
		{
			var result = new string[Count];

			for(int i = 0; i < Count; i++)
			{
				result[i] = RandomStringAs[i] + "\n" + RandomStringBs[i] + "\n" + RandomStringCs[i];
			}

			return result;
		}

		[Benchmark(Baseline = true)]
		public string[] StringCreateFormatted()
		{
			var result = new string[Count];

			for(int i = 0; i < Count; i++)
			{
				result[i] = $"{RandomStringAs[i]}\n{RandomStringBs[i]}\n{RandomStringCs[i]}";
			}

			return result;
		}
	}
}