﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class BranchPredictionExample
	{
		const int N = 32767;
		readonly int[] SortedArray, UnsortedArray;

		public BranchPredictionExample()
		{
			var random = new Random(0);
			UnsortedArray = new int[N];
			SortedArray = new int[N];
			for(int i = 0; i < N; i++)
				SortedArray[i] = UnsortedArray[i] = random.Next(256);
			Array.Sort(SortedArray);
		}

		static int Branch(int[] data)
		{
			int sum = 0;
			for(int i = 0; i < N; i++)
				if(data[i] >= 128)
					sum += data[i];
			return sum;
		}

		static int Branchless(int[] data)
		{
			int sum = 0;
			for(int i = 0; i < N; i++)
			{
				int t = (data[i] - 128) >> 31;
				sum += ~t & data[i];
			}

			return sum;
		}

		static int Ternary(int[] data)
		{
			int sum = 0;
			for(int i = 0; i < N; i++)
			{
				var t = data[i];
				sum += t >= 128 ? t : 0;
			}

			return sum;
		}
		
		[Benchmark]
		public int SortedBranch() => Branch(SortedArray);

		[Benchmark(Baseline = true)]
		public int UnsortedBranch() => Branch(UnsortedArray);

		[Benchmark]
		public int SortedBranchless() => Branchless(SortedArray);

		[Benchmark]
		public int UnsortedBranchless() => Branchless(UnsortedArray);

		[Benchmark]
		public int SortedTernary() => Ternary(SortedArray);

		[Benchmark]
		public int UnsortedTernary() => Ternary(UnsortedArray);
	}
}