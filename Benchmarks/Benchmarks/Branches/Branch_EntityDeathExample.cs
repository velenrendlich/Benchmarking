﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class Branch_EntityDeathExample
	{
		public struct Entity
		{
			public float Health;
			public bool IsAlive;
		}

		public struct Damage
		{
			public float Amount;
		}

		const int Count = 100_000;
		Entity[] Entities = new Entity[Count];
		Damage[] Damages = new Damage[Count];

		public Branch_EntityDeathExample()
		{
			var random = new Random(0);
			for(int i = 0; i < Count; i++)
			{
				Entities[i] = new Entity
				{
					Health = random.Next(0, 100),
					IsAlive = true
				};
				Damages[i] = new Damage
				{
					Amount = random.Next(0, 100)
				};
			}
		}

		void RunSimulation_Branch()
		{
			for(int i = 0; i < Count; i++)
			{
				Entities[i].Health -= Damages[i].Amount;
				if(Entities[i].Health <= 0)
				{
					Entities[i].IsAlive = false;
				}
			}
		}

		void RunSimulation_Branchless()
		{
			for(int i = 0; i < Count; i++)
			{
				Entities[i].Health -= Damages[i].Amount;
				Entities[i].IsAlive = Entities[i].Health <= 0;
			}
		}

		[Benchmark]
		public void Branch() => RunSimulation_Branch();

		[Benchmark]
		public void Branchless() => RunSimulation_Branchless();
	}
}