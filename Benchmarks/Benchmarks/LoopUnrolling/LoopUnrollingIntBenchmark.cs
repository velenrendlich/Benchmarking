﻿using System;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	[ShortRunJob]
	public class LoopUnrollingIntBenchmark
	{
		int Count = 10_000_000;
		int[] Array;

		public LoopUnrollingIntBenchmark()
		{
			Array = new int[Count];
			var random = new Random(0);

			for(int i = 0; i < Count; i++)
			{
				Array[i] = random.Next(0, 100);
			}
		}

		[Benchmark(Baseline = true)]
		public int SumDefault()
		{
			var sum = 0;
			for(int i = 0; i < Array.Length; i++)
			{
				sum += Array[i];
			}

			return sum;
		}

		[Benchmark]
		public int SumUnrolled2_Var0()
		{
			var sum = 0;
			var i = 0;

			for(; i < Array.Length; i += 2)
			{
				sum = (sum + Array[i]) + Array[i + 1];
			}
			// Finish remaining
			for(; i < Array.Length; i++)
			{
				sum += Array[i];
			}
			
			return sum;
		}
		
		[Benchmark]
		public int SumUnrolled4_Var0()
		{
			var sum = 0;
			var i = 0;

			for(; i < Array.Length; i += 4)
			{
				sum = sum + Array[i] + Array[i + 1] + Array[i + 2] + Array[i + 3];
			}
			// Finish remaining
			for(; i < Array.Length; i++)
			{
				sum += Array[i];
			}
			
			return sum;
		}
		
		[Benchmark]
		public int SumUnrolled8_Var0()
		{
			var sum = 0;
			var i = 0;

			for(; i < Array.Length; i += 8)
			{
				sum = sum + Array[i] + Array[i + 1] + Array[i + 2] + Array[i + 3] + Array[i + 4] + Array[i + 5] + Array[i + 6] + Array[i + 7];
			}
			// Finish remaining
			for(; i < Array.Length; i++)
			{
				sum += Array[i];
			}
			
			return sum;
		}
		
		[Benchmark]
		public int SumUnrolled2_Var1()
		{
			var sum0 = 0;
			var sum1 = 0;
			var i = 0;

			for(; i < Array.Length; i += 2)
			{
				sum0 += Array[i];
				sum1 += Array[i + 1];
			}
			// Finish remaining
			for(; i < Array.Length; i++)
			{
				sum0 += Array[i];
			}
			
			return sum0 + sum1;
		}
		
		[Benchmark]
		public int SumUnrolled4_Var1()
		{
			var sum0 = 0;
			var sum1 = 0;
			var sum2 = 0;
			var sum3 = 0;
			var i = 0;

			for(; i < Array.Length; i += 4)
			{
				sum0 += Array[i];
				sum1 += Array[i + 1];
				sum2 += Array[i + 2];
				sum3 += Array[i + 3];
			}
			// Finish remaining
			for(; i < Array.Length; i++)
			{
				sum0 += Array[i];
			}
			
			return sum0 + sum1 + sum2 + sum3;
		}
	}
}