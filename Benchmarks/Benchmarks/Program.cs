﻿using System;
using BenchmarkDotNet.Running;
using Benchmarks.CompilerOptimizationBlockers;
using Benchmarks.String;

namespace Benchmarks
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Executing Benchmark...");
			var summary = BenchmarkRunner.Run<StringBenchmarks>();
			Console.WriteLine("Done.");
			Console.ReadLine();
		}
	}
}